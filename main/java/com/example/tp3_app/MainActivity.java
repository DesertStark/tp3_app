package com.example.tp3_app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.example.tp3_app.data.City;
import com.example.tp3_app.data.WeatherDbHelper;
import com.example.tp3_app.webservice.JSONResponseHandler;
import com.example.tp3_app.webservice.WebServiceUrl;

import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    SimpleCursorAdapter cursorAdapter;
    WeatherDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ListView citiesList = findViewById(R.id.citiesList);

        dbHelper = new WeatherDbHelper(this);
        dbHelper.populate();
        cursorAdapter = new SimpleCursorAdapter(this,
                R.layout.row,
                dbHelper.fetchAllCities(),
                new String[]{WeatherDbHelper.COLUMN_ICON, WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.COLUMN_COUNTRY, WeatherDbHelper.COLUMN_TEMPERATURE},
                new int[]{R.id.imageViewRow, R.id.cName, R.id.cCountry, R.id.temperature});

        cursorAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
            public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                if (view.getId() == R.id.imageViewRow) {
                    if (cursor.getString(columnIndex) != null) {
                        String image = cursor.getString(columnIndex);
                        ImageView imageView = (ImageView) view;
                        imageView.setImageDrawable(getResources().getDrawable(getResources()
                                .getIdentifier("@drawable/" + "icon_" + image, null, getPackageName())));
                    }
                    return true;
                } else {
                    return false;
                }
            }
        });
        citiesList.setAdapter(cursorAdapter);

        final SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                @SuppressLint("StaticFieldLeak")
                AsyncTask asyncTask = new AsyncTask() {
                    @SuppressLint("WrongThread")
                    @Override
                    protected Object doInBackground(Object[] objects) {
                        try {
                            for (int i = 0 ; i < citiesList.getCount() ; ++i) {
                                City city = dbHelper.cursorToCity((Cursor) citiesList.getItemAtPosition(i));
                                URL url = WebServiceUrl.build(city.getName(), city.getCountry());
                                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                JSONResponseHandler jsonResponseHandler = new JSONResponseHandler(city);
                                jsonResponseHandler.readJsonStream(connection.getInputStream());
                                dbHelper.updateCity(city);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        super.onPostExecute(o);
                        cursorAdapter.changeCursor(dbHelper.fetchAllCities());
                        cursorAdapter.notifyDataSetChanged();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }.execute();
            }
        });

        // Update
        citiesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                intent.putExtra(City.TAG, dbHelper.cursorToCity((Cursor) parent.getItemAtPosition(position)));
                startActivityForResult(intent, Code.UPDATE_CITY);
            }
        });

        FloatingActionButton addBtn = findViewById(R.id.addBtn);

        // Add
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivityForResult(intent, Code.ADD_CITY);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        City city;
        switch (resultCode) {
            case Code.ADD_CITY:
                assert data != null;
                city = data.getParcelableExtra("city");
                dbHelper.addCity(city);
                break;
            case Code.UPDATE_CITY:
                assert data != null;
                city = data.getParcelableExtra("city");
                dbHelper.updateCity(city);
                break;
        }
        cursorAdapter.changeCursor(dbHelper.fetchAllCities());
        cursorAdapter.notifyDataSetChanged();
    }


}

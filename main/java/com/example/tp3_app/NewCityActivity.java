package com.example.tp3_app;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.tp3_app.data.City;
import com.example.tp3_app.data.WeatherDbHelper;

public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.updateBtn);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                WeatherDbHelper weatherDbHelper = new WeatherDbHelper(NewCityActivity.this);
                if (textName.getText().toString().equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewCityActivity.this);
                    builder.setTitle("Ajout impossible").setMessage("Le nom de la ville doit être non vide").show();
                    return;
                }
                if (textCountry.getText().toString().equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewCityActivity.this);
                    builder.setTitle("Ajout impossible").setMessage("Le nom du pays doit être non vide").show();
                    return;
                }
                Intent intent = new Intent(NewCityActivity.this, MainActivity.class);
                intent.putExtra("city", new City(textName.getText().toString(), textCountry.getText().toString()));
                setResult(Code.ADD_CITY, intent);
                finish();
            }
        });
    }


}
